#PAGE COMPONENT
page =  new PageComponent 
	width: 320
	height: 320
	backgroundColor: "#FFF"
	scrollHorizontal: true
	scrollVertical: false
	borderRadius: 320/2
	
page.center()
page.borderRadius = page.width/2
page.x += 2.5

# ADDING MAIN UI SCREENS TO PAGE COMPONENT
screenLayers = Framer.Importer.load "imported/screens"
screen_keys = Object.keys(screenLayers)
for i in screen_keys
	screenLayers[i].y = -180

# # Create layers in a for-loop
layers = {}
for i in screen_keys
	layer = new Layer 
		width: 320
		height: 320
		image: screenLayers[i].image
		backgroundColor: "#fff"
		borderRadius: 320/2
		opacity: 0
		
	layers[i] = layer
	
layers['time'].states.add
	'heart':
		image: screenLayers['heart'].image
	'map':
		image: screenLayers['map'].image
	
# # Staging
page.addPage(layers['start'])
page.addPage(layers['time'])
page.addPage(layers['ride'])
page.currentPage.opacity = 1
	
layers['start'].on "click", ->
	layers['time'].states.switch('default')
	page.snapToPage(layers['time'])
	
layers['time'].on "click", ->
	layers['time'].states.next()
		
layers['ride'].on "click", ->
	page.snapToPage(layers['start'])
	
# Update pages
page.on "change:currentPage", ->
	page.previousPage.animate 
		properties:
			opacity: 0.3
			scale: 0.8
		time: 0.4
		
	page.currentPage.animate 
		properties:
			opacity: 1
			scale: 1
		time: 0.4

# ADD SKIN ON TOP OF EVERYTHING ELSE
moto360 = new Layer
	width: 600, height: 900
	image: "images/moto_moto360-mask.png"
moto360.scaleX = 0.60
moto360.scaleY = 0.60
moto360.center()

Events.wrap(window).addEventListener "resize", (event) ->
	moto360.center()
	page.center()
	page.x += 2.5
